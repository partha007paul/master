package com.example.demo.restservice


import com.example.demo.BuildConfig
import com.example.demo.activity.BaseActivity
import com.iserve.askalan.constant.Constant
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit


open class RestServiceOld {

    companion object {
        lateinit var restInterface: RestInterface

      operator fun invoke(baseActivity: BaseActivity?): RestInterface? {
          val requestInters = Interceptor{
              chain ->
              val url  = chain.request()
                  .url()
                  .newBuilder()
//                  .addQueryParameter()
                  .build()

              val req = chain.request()
                  .newBuilder()
//                  .addHeader()
                  .url(url)
                  .build()
              return@Interceptor chain.proceed(req)
          }

            var defaultHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInters)
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .build()


          val clientBuilder = OkHttpClient.Builder()

          if (BuildConfig.DEBUG) {
              val httpLoggingInterceptor = HttpLoggingInterceptor()
              httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
              clientBuilder.addInterceptor(httpLoggingInterceptor)
          }

          restInterface =  Retrofit.Builder()
              .client(defaultHttpClient)
              .baseUrl(RestInterface.BASE_URL)
               .addCallAdapterFactory(CoroutineCallAdapterFactory())
              .addConverterFactory(GsonConverterFactory.create())
              .build()
              .create(RestInterface::class.java)
          return restInterface
      }
    }
}
