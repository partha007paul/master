package com.example.demo.restservice


import com.example.demo.activity.call_api.data_model.UserDataModel
import com.iserve.askalan.constant.Constant
import retrofit2.http.GET
import retrofit2.http.QueryMap
import io.reactivex.Observable

interface RestInterfaceRxKortlin {
    companion object {
        val BASE_URL = Constant.BASE_URL
    }



    @GET("api/users")
    fun getData(@QueryMap params: Map<String, String>) : Observable<List<UserDataModel>>





    //    @FormUrlEncoded
//    @POST("api/login")
//    fun loginUser(@FieldMap params: Map<String, String>): Call<ResponseBody>
//
//    @FormUrlEncoded
//    @POST("api/register")
//    fun registerUser(@FieldMap params: Map<String, String>): Call<ResponseBody>
//
//    @GET("api/guest/job/list?")
//    fun guestJobList(@QueryMap params: Map<String, String>): Call<ResponseBody>
//
//    @GET("api/guest/all/category?")
//    fun getAllCategory(): Call<ResponseBody>

//    @FormUrlEncoded
//    @POST("ticket/scan")
//    fun getScan(@FieldMap params: Map<String, String>): Call<ResponseBody>
//
//    @GET("ticket")
//    fun getHistory(): Call<ResponseBody>
//
//    @GET("ticket/reset")
//    fun getReset(): Call<ResponseBody>



    //@FormUrlEncoded
    //    @POST("update_profile")
    //    Call<ResponseBody> update_profile(@FieldMap Map<String, String> params);
    //
    //
    //    @POST("show_user_info")
    //    Call<ResponseBody> show_user_info();
    //
    //    @POST("driver_register")
    //    Call<ResponseBody> driver_register();
    //
    //
    //    @FormUrlEncoded
    //    @POST("online")
    //    Call<ResponseBody> show_user_online(@FieldMap Map<String, String> params);
    //
    //
    //    @FormUrlEncoded
    //    @POST("vehicle_register")
    //    Call<ResponseBody> show_vehicle_register(@FieldMap Map<String, String> params);//,@FieldMap Map<String, Integer> params1);
    //
    //    @FormUrlEncoded
    //    @POST("update_driver_data")
    //    Call<ResponseBody> set_update_driver_data(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("driver_confirm")
    //    Call<ResponseBody> driver_confirm(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("update_coordinate")
    //    Call<ResponseBody> show_user_show_open_requests(@FieldMap Map<String, String> params);
    //
    //
    //    @FormUrlEncoded
    //    @POST("cancel_trip")
    //    Call<ResponseBody> cancel_trip(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("show_open_requests")
    //    Call<ResponseBody> show_open_requests(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("transaction_history")
    //    Call<ResponseBody> getGanancias(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("show_my_trip")
    //    Call<ResponseBody> getshow_my_trip(@FieldMap Map<String, String> params);

    //    @GET("login.php")
    //    Call<ResponseBody> getLogin(@QueryMap Map<String, String> params);
    //
    //    @GET("app-user-reg.php")
    //    Call<ResponseBody> getSignUp(@QueryMap Map<String, String> params);
    //
    //    @GET("menu-category.php")
    //    Call<ResponseBody> getCategory(@QueryMap Map<String, String> params);
    //
    //
    //    @GET("qrcode.php")
    //    Call<ResponseBody> getQrcode(@QueryMap Map<String, String> params);
    //
    //    @GET("menu-list.php")
    //    Call<ResponseBody> getMenu(@QueryMap Map<String, String> params);
    //
    //    @GET("addons.php")
    //    Call<ResponseBody> getAddons(@QueryMap Map<String, String> params);
    //
    //    @GET("table-details.php")
    //    Call<ResponseBody> getTable(@QueryMap Map<String, String> params);
    //


    //    @GET("wp/v2/listing-category")
    //    Call<ResponseBody> getCategory(@QueryMap Map<String, String> params);
    //
    //    @GET("app-login/register")
    //    Call<ResponseBody> getRegister(@QueryMap Map<String, String> params);
    //
    //    @GET("app-login/update")
    //    Call<ResponseBody> getUpdate(@QueryMap Map<String, String> params);
    //
    //    @GET("app-login/resset_password")
    //    Call<ResponseBody> getResset_password(@QueryMap Map<String, String> params);
    //
    //    @GET("get-review/listing")
    //    Call<ResponseBody> listing(@QueryMap Map<String, String> params);
    //
    //    @GET("get-listing/listing")
    //    Call<ResponseBody> getListing();
    //
    //    @GET("wp/v2/location")
    //    Call<ResponseBody> getLocationList();
    ////    https://blog.webskitters.com/alleno/wp-json/get-listing/listing?trend=campaign&cat_id=1670&id=1616
    //    @GET("get-listing/listing")
    //    Call<ResponseBody> getListingDetails(@QueryMap Map<String, String> params);
    //
    ////    https://blog.webskitters.com/alleno/wp-json/get-listing/listing?trend=trending
    ////    @GET("get-listing/listing")
    ////    Call<ResponseBody> getListingDetails(@QueryMap Map<String, String> params);
    //
    //    @GET("get-review/listing")
    //    Call<ResponseBody> getReviewDetails();
    //
    //    @Multipart
    //    @POST("app-login/image_upload")
    //    Call<ResponseBody> postProfilePicture(@Part("id") RequestBody id,
    //                                          @Part List<MultipartBody.Part> filesAry);
    //    //    https://blog.webskitters.com/alleno/wp-json/post-review/review
    //    @Multipart
    //    @POST("post-review/review")
    //    Call<ResponseBody> app_Review(@Part("comment_post_ID") RequestBody comment_post_ID,
    //                                  @Part("post_title") RequestBody post_title,
    //                                  @Part("post_description") RequestBody post_description,
    //                                  @Part("rating") RequestBody rating,
    //                                  @Part("rating_1") RequestBody rating1,
    //                                  @Part("rating_2") RequestBody rating2,
    //                                  @Part("rating_3") RequestBody rating3,
    //                                  @Part("rating_4") RequestBody rating4,
    //                                  @Part("User_ID") RequestBody User_ID,
    //                                  @Part List<MultipartBody.Part> filesAry);

    ////    https://blog.webskitters.com/alleno/wp-json/post-review/reaction
    //
    //    @FormUrlEncoded
    //    @POST("post-review/reaction")
    //    Call<ResponseBody> app_reaction(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("post-message/listingOwner")
    //    Call<ResponseBody> listingOwner(@FieldMap Map<String, String> params);
    //    @FormUrlEncoded
    //    @POST("post-claim/claim")
    //    Call<ResponseBody> getClaimNow(@FieldMap Map<String, String> params);


    //    //https://blog.webskitters.com/mushi/wp-json/app-login/resset_password?user_login=pratap.maity@webskitters.com
    //    @GET("app-login/resset_password")
    //    Call<ResponseBody> getResset_password(@QueryMap Map<String, String> params);
    //
    //
    //    @FormUrlEncoded
    //    @POST("wc/v2/customers/{id}?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4")
    //    Call<ResponseBody> app_ChamgePassword(@Path("id") String id, @FieldMap Map<String, String> params);
    //
    //    @POST("wc/v2/customers/{id}?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4")
    //    @Headers("Content-Type: application/json")
    //    Call<ResponseBody> app_edit_Profile(@Path("id") String id, @Body SignUpResponse paramString);
    //
    //    @POST("wc/v2/customers?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4")
    //    @Headers("Content-Type: application/json")
    //    Call<ResponseBody> app_Registration1(@Body SignUpResponse paramObject);
    //
    //    @POST("wc/v2/orders/{id}?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4")
    //    @Headers("Content-Type: application/json")
    //    Call<ResponseBody> app_order_canceled(@Path("id") String id, @Body OrderCanceled paramObject);
    //
    //    //https://blog.webskitters.com/mushi/wp-json/wc/v2/orders?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4
    //    @POST("wc/v2/orders?consumer_key=ck_72819a85723748407bc17de0fb26af8b329a88d0&consumer_secret=cs_c788ea191fb38f57a2fd16d8964afa45c886c4f4")
    //    @Headers("Content-Type: application/json")
    //    Call<ResponseBody> app_orders(@Body SentOrder sentOrder);
    //
    //    @Multipart
    //    @POST("app-login/image_upload")
    //    Call<ResponseBody> app_upload(@Part("id") RequestBody id,
    //                                  @Part List<MultipartBody.Part> filesAry);
    //    @FormUrlEncoded
    //    @POST("login")
    //    Call<ResponseBody> app_Login(@FieldMap Map<String, String> params);

    //    @GET("category/list")
    //    Call<ResponseBody> getCategoryList();
    //
    //    @FormUrlEncoded
    //    @POST("user/register")
    //    Call<ResponseBody> AppRegister(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("user/login")
    //    Call<ResponseBody> AppLogin(@FieldMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("user/profile/update")
    //    Call<ResponseBody> updateProfile(@FieldMap Map<String, String> params);
    //
    //    @GET("story/list")
    //    Call<ResponseBody> getStoryList();
    //
    //    @GET("item")
    //    Call<ResponseBody> getItem(@QueryMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("favourite")
    //    Call<ResponseBody> getItemFavourite(@FieldMap Map<String, String> params);
    //
    //    @GET("favourite/list")
    //    Call<ResponseBody> getFavouriteItem1();
    //
    //
    //    @GET("favourite/list")
    //    Call<ResponseBody> getFavouriteItem(@QueryMap Map<String, String> params);
    //
    //
    //
    //    @GET("item/search")
    //    Call<ResponseBody> getItemSearch(@QueryMap Map<String, String> params);
    //
    //    @FormUrlEncoded
    //    @POST("comment/add")
    //    Call<ResponseBody> getComment(@FieldMap Map<String, String> params);
    //
    //    @GET("archive")
    //    Call<ResponseBody> getArchive();

}
