//package com.iserve.askalan.restservice
//
//
//import com.example.demo.BuildConfig
//import com.example.demo.activity.BaseActivity
//import com.example.demo.restservice.RestInterface
//import com.google.gson.Gson
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//
//import java.util.concurrent.TimeUnit
//
//
//open class RestService {
//    var restInterface: RestInterface
//    private var gson: Gson? = null
//
//
//    internal var defaultHttpClient = OkHttpClient.Builder()
//        .addInterceptor { chain ->
//            val authorisedRequest = chain.request().newBuilder()
//                .build()
//            chain.proceed(authorisedRequest)
//        }.connectTimeout(100, TimeUnit.SECONDS)
//        .readTimeout(100, TimeUnit.SECONDS).build()
//
//
//
//    init {
//        val clientBuilder = OkHttpClient.Builder()
//        if (BuildConfig.DEBUG) {
//            val httpLoggingInterceptor = HttpLoggingInterceptor()
//            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
//            clientBuilder.addInterceptor(httpLoggingInterceptor)
//        }
//        restInterface = Retrofit.Builder()
//            .baseUrl(RestInterface.BASE_URL)
//            .client(defaultHttpClient)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//            .create(RestInterface::class.java)
//    }
//
//    companion object {
//
//        private var restService: RestService? = null
//        private var baseActivity_: BaseActivity? = null
//
//        fun getInstance(baseActivity: BaseActivity): RestService {
//            if (restService == null) {
//                restService = RestService()
//                baseActivity_ = baseActivity
//
//            }
//
//            return restService as RestService
//        }
//    }
//}
