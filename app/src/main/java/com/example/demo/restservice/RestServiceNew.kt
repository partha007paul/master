//package com.example.demo.restservice
//
//import com.example.demo.activity.call_api.user_data_model.UserDataModel
//import com.iserve.askalan.constant.Constant
//import com.squareup.moshi.Moshi
//import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
//import kotlinx.coroutines.Deferred
//import retrofit2.Retrofit
//import retrofit2.converter.moshi.MoshiConverterFactory
//import retrofit2.http.GET
//import retrofit2.http.QueryMap
//
//
//private  val BASE_URL = Constant.BASE_URL
//
///**
// * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
// * full Kotlin compatibility.
// */
//private val moshi = Moshi.Builder()
//    .add(KotlinJsonAdapterFactory())
//    .build()
//
///**
// * Use the Retrofit builder to build a retrofit object using a Moshi converter with our Moshi
// * object.
// */
//private val retrofit = Retrofit.Builder()
//    .addConverterFactory(MoshiConverterFactory.create(moshi))
//    .baseUrl(BASE_URL)
//    .build()
//
///**
// * A public interface that exposes the [getPhotosAsync] method
// */
//interface MarsApiService {
//    /**
//     * Returns a [List] of [MarsPhoto] and this method can be called from a Coroutine.
//     * The @GET annotation indicates that the "photos" endpoint will be requested with the GET
//     * HTTP method
//     */
//    @GET("api/users")
//    suspend fun getPhotosAsync(@QueryMap params: Map<String, String>): Deferred<UserDataModel>
//}
//
///**
// * A public Api object that exposes the lazy-initialized Retrofit service
// */
//object MarsApi {
//
//    val retrofitService: MarsApiService by lazy { retrofit.create(MarsApiService::class.java) }
//}
