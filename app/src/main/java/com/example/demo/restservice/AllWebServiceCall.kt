//package com.iserve.askalan.restservice
//
//
//import com.example.demo.activity.BaseActivity
//import okhttp3.ResponseBody
//import retrofit2.Call
//import retrofit2.Callback
//import retrofit2.Response
//
//
///**
// * Created by android on 8/3/18.
// */
//
//class AllWebServiceCall(
//    private val baseActivity: BaseActivity,
//    private var getDepartment: Call<ResponseBody>,
//    private val webInterface: WebInterface,
//    methodName: String
//) {
//    private var methodName = ""
//
//    init {
//        this.methodName = methodName
//    }
//
//    fun webServiceCall() {
//        getDepartment.enqueue(object : Callback<ResponseBody> {
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                try {
//                    if (response.code() in 200..399 && response != null) {
//                        responce = response.body()!!.string()
//                        webInterface.resultSuccess(responce, methodName)
//                    } else {
//                        responce = response.errorBody()!!.string()
//                            webInterface.failureSuccess(responce)
//                    }
//                } catch (e: Exception) {
//                    baseActivity.hideProgress()
//                    e.printStackTrace()
//                }
//
//            }
//
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                webInterface.failureSuccess(t.message.toString())
//
//
//            }
//        })
//
//    }
//
//    companion object {
//        private var responce = ""
//    }
//}
