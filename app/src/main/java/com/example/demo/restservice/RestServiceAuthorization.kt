//package com.example.demo.restservice
//
//
//import com.example.demo.BuildConfig
//import com.example.demo.activity.BaseActivity
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import java.util.concurrent.TimeUnit
//
//
//open class RestServiceAuthorization {
//    var restInterface: RestInterface
//
//
//    internal var defaultHttpClient = OkHttpClient.Builder()
//        .addInterceptor { chain ->
//            val authorisedRequest = chain.request().newBuilder()
////                .header("authorization", "Bearer "+ baseActivity_!!.application.getLoginDetails()!!.data!!.token)
//                .header("role", "")
//                .header("locale", "en ")
//                .build()
//            chain.proceed(authorisedRequest)
//        }.connectTimeout(100, TimeUnit.SECONDS)
//        .readTimeout(100, TimeUnit.SECONDS).build()
//
//
//
//    init {
//        val clientBuilder = OkHttpClient.Builder()
//        if (BuildConfig.DEBUG) {
//            val httpLoggingInterceptor = HttpLoggingInterceptor()
//            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
//            clientBuilder.addInterceptor(httpLoggingInterceptor)
//        }
//        restInterface = Retrofit.Builder()
//            .baseUrl(RestInterface.BASE_URL)
//            .client(defaultHttpClient)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//            .create(RestInterface::class.java)
//    }
//
//    companion object {
//
//        private var restService: RestServiceAuthorization? = null
//        private var baseActivity_: BaseActivity? = null
//
//        fun getInstance(baseActivity: BaseActivity): RestServiceAuthorization {
//            if (restService == null) {
//                restService = RestServiceAuthorization()
//                baseActivity_ = baseActivity
//
//            }
//
//            return restService as RestServiceAuthorization
//        }
//    }
//}
