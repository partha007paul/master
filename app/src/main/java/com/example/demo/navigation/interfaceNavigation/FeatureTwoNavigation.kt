package com.example.demo.navigation.interfaceNavigation

import com.example.demo.fragment.base.Navigation


/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
interface FeatureTwoNavigation : Navigation {
    fun navigateToScreenTwo()
}