package com.example.demo.navigation

import com.example.demo.R
import com.example.demo.fragment.base.BaseNavigator
import com.example.demo.navigation.interfaceNavigation.SplashScreenNavigation

/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
internal class SplashScreenNavigationImpl : BaseNavigator(), SplashScreenNavigation {
    override fun navigateToFeatureOne() {
        navController?.navigate(R.id.action_global_feature_one)
    }

    override fun navigateToFeatureTwo() {
        navController?.navigate(R.id.action_global_feature_two)
    }
}