package com.example.demo.navigation

import com.example.demo.R
import com.example.demo.fragment.base.BaseNavigator
import com.example.demo.navigation.interfaceNavigation.FeatureOneNavigation

/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
internal class FeatureOneNavigationImpl : BaseNavigator(), FeatureOneNavigation {
    override fun navigateToScreenTwo() {
        navController?.navigate(R.id.action_fragmentFeatureOneScreenOne_to_fragmentFeatureOneScreenTwo)
    }

    override fun navigateToFeatureTwo() {
        navController?.navigate(R.id.action_global_feature_two)
    }
}