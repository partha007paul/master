package com.example.demo.navigation

import com.example.demo.R
import com.example.demo.fragment.base.BaseNavigator
import com.example.demo.navigation.interfaceNavigation.FeatureTwoNavigation

/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
internal class FeatureTwoNavigationImpl : BaseNavigator(), FeatureTwoNavigation {
    override fun navigateToScreenTwo() {
        navController?.navigate(R.id.action_fragmentFeatureOneScreenOne_to_fragmentFeatureTwoScreenTwo)

    }
}