package com.example.demo.di.modules

import com.example.demo.navigation.FeatureOneNavigationImpl
import com.example.demo.navigation.FeatureTwoNavigationImpl
import com.example.demo.navigation.SplashScreenNavigationImpl
import com.example.demo.navigation.interfaceNavigation.FeatureOneNavigation
import com.example.demo.navigation.interfaceNavigation.FeatureTwoNavigation
import com.example.demo.navigation.interfaceNavigation.SplashScreenNavigation
import org.koin.dsl.module


/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
val navigatorModule = module {

    factory<SplashScreenNavigation> {
        SplashScreenNavigationImpl()
    }

    factory<FeatureOneNavigation> {
        FeatureOneNavigationImpl()
    }

    factory<FeatureTwoNavigation> {
        FeatureTwoNavigationImpl()
    }
}