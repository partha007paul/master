package com.example.demo.application

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.iserve.askalan.constant.Constant
import com.example.demo.di.DependenciesInitializer
import com.example.demo.interface_all.db.AppDatabase


open class AppApplication : Application() {
    private var sharedPreferences: SharedPreferences? = null
    //    internal lateinit var dataComponent: DataComponent
    var database: AppDatabase? = null

    companion object {
        private lateinit var app: AppApplication
        fun getApp(): AppApplication {
            return app;
        }
    }


    override fun onCreate() {
        super.onCreate()
        this.sharedPreferences = getSharedPreferences(Constant.Userdata.USER_PREF.name, Context.MODE_PRIVATE)
        app =this


        DependenciesInitializer.invoke(this)
        database = AppDatabase.invoke(context = this@AppApplication)



//        initDataComponent()
//        dataComponent.inject(this)

        //        FirebaseApp.initializeApp(this);
//                FacebookSdk.sdkInitialize(getApplicationContext())
        //        AppEventsLogger.activateApp(this);
//                printHashKey();

    }

//    fun printHashKey() {
//        // Add code to print out the key hash
//        try {
//            val info = packageManager.getPackageInfo(
//                "com.iserve.askalan" ,
//                        PackageManager . GET_SIGNATURES
//            )
//            for (signature in info.signatures) {
//                val md = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
//                //                OHfOV3okJST4ao60cL878r4yaPA=
//            }
//        } catch (e: PackageManager.NameNotFoundException) {
//            Log.d("Error:- KeyHash:- ", e.message)
//        } catch (e: NoSuchAlgorithmException) {
//            Log.d("Error:- KeyHash:-", e.message)
//
//        }
//    }

//        public void printHashKey(){
//            // Add code to print out the key hash
//            try {
//                PackageInfo info = getPackageManager().getPackageInfo(
//                        "com.mytride.",
//                        PackageManager.GET_SIGNATURES);
//                for (Signature signature : info.signatures) {
//                    MessageDigest md = MessageDigest.getInstance("SHA");
//                    md.update(signature.toByteArray());
//                    Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//    //                ie7F/QMJIeRqqqJTiyJJj1g6KOQ=
//                }
//            } catch (PackageManager.NameNotFoundException e) {
//
//            } catch (NoSuchAlgorithmException e) {
//
//            }
//        }



//    private fun initDataComponent() {
//        dataComponent = DaggerDataComponent.builder()
//            .dataModule(DataModule(this))
//            .build()
//    }

//    fun getDataComponent(): DataComponent {
//        return dataComponent
//    }
//    /////////////Login Responce///////////
//
//    fun setLoginDetails(loginDetails: String) {
//        val editor = sharedPreferences!!.edit()
//        this.loginDetails = loginDetails
//        editor.putString(Constant.Userdata.USER_LOGIN.name, this.loginDetails)
//        editor.commit()
//    }

//    fun getLoginDetails(): LoginResponce? {
//        val loginDetails = sharedPreferences!!.getString(Constant.Userdata.USER_LOGIN.name, this.loginDetails)
//        if(loginDetails!=null)
//            return Gson().fromJson<LoginResponce>(loginDetails, LoginResponce::class.java)
//        else
//            return  null
//    }   /////////////scan Responce///////////
//
//    fun setScanResponce(scanResponce: String) {
//        val editor = sharedPreferences!!.edit()
//        this.scanResponce = scanResponce
//        editor.putString(Constant.Userdata.SESSION.name, this.scanResponce)
//        editor.commit()
//    }
//
//    fun getScanResponce(): ScanResponce? {
//        val scanResponce = sharedPreferences!!.getString(Constant.Userdata.SESSION.name, this.scanResponce)
//        if(scanResponce!=null)
//            return Gson().fromJson<ScanResponce>(scanResponce, ScanResponce::class.java)
//        else
//            return  null
//    }


//    fun setLoginDetails(loginDetails: String) {
//        val editor = sharedPreferences!!.edit()
//        this.loginDetails = loginDetails
//        editor.putString(Constant.Userdata.USER_LOGIN.name, this.loginDetails)
//        editor.commit()
//    }

//    fun getLoginDetails(): LoginResponse {
//        val loginDetails = sharedPreferences!!.getString(Constant.Userdata.USER_LOGIN.name, this.loginDetails)
//        return Gson().fromJson(loginDetails, LoginResponse::class.java)
//    }
}