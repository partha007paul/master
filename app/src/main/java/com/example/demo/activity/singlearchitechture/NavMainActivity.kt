package com.example.demo.activity.singlearchitechture

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.example.demo.R
import com.example.demo.activity.BaseActivity

class NavMainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_main)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.navigation).navigateUp()
    }
}