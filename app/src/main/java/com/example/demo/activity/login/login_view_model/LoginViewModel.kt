package com.example.demo.activity.login.login_view_model



import android.app.Application
import android.content.Intent
import android.os.Build
import android.provider.MediaStore
import android.view.View
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.lifecycle.AndroidViewModel
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.demo.MainDemoActivity
import com.example.demo.activity.login.LoginActivity


open class LoginViewModel(application: Application) : AndroidViewModel(application) {
    lateinit var mBaseActivity : BaseActivity

    fun onClick(v: View): Unit {
        when (v.id) {

            R.id.btGallery -> {
                mBaseActivity.openActivity(mBaseActivity, MainDemoActivity::class.java,false)

            }
        }
    }


}



