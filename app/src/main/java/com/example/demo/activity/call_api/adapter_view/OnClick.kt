package com.example.demo.activity.call_api.adapter_view

import androidx.navigation.NavController

interface OnClick {
    fun position(navPosition: Int)
}