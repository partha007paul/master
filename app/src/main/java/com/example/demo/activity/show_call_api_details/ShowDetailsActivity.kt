package com.example.demo.activity.show_call_api_details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.show_call_api_details.view_model.ShowDetailsViewModel
import com.example.demo.databinding.ActivityShowDetailsBinding

class ShowDetailsActivity : BaseActivity() {
    var binding: ActivityShowDetailsBinding? = null
    lateinit var mShowDetailsViewModel : ShowDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()

    }

    private fun bind(): View {
        binding = DataBindingUtil.setContentView(this@ShowDetailsActivity, R.layout.activity_show_details)
        mShowDetailsViewModel = ViewModelProviders.of(this).get(ShowDetailsViewModel::class.java)
        mShowDetailsViewModel.mBaseActivity = this@ShowDetailsActivity
        mShowDetailsViewModel.mDataModel = mDataModel
        binding!!.lifecycleOwner = this@ShowDetailsActivity
        binding!!.apiDataShowDetails = mShowDetailsViewModel
        return binding!!.root
    }
}