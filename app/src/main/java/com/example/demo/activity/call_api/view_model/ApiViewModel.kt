package com.example.demo.activity.call_api.view_model



import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.data_model.DataModel
import com.example.demo.activity.login.LoginActivity
import com.example.demo.restservice.RestServiceOld
import com.iserve.askalan.constant.Constant
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import io.reactivex.Observable
import io.reactivex.Observer
import java.util.*

open class ApiViewModel(application: Application) : AndroidViewModel(application) {

    var count: Int = 0
    lateinit var mBaseActivity: BaseActivity

    private val _status = MutableLiveData<Constant.MarsApiStatus>()

    val status: LiveData<Constant.MarsApiStatus> = _status

    private val _photos = MutableLiveData<List<DataModel>>()
    val localPhotos: ArrayList<DataModel> = ArrayList<DataModel>()
    val photos: LiveData<List<DataModel>> = _photos

    fun onClick(v: View): Unit {
        when (v.id) {
            R.id.btOnclick -> {
                count++
                callApi(count.toString())
            }
            R.id.btOnclickList -> {
                startRStream()
//                callAdd()
            }
            R.id.btLogin -> {

                mBaseActivity.openActivity(mBaseActivity, LoginActivity::class.java,false)
            }
        }
    }

    private fun callApi(mString: String) {
        val params = HashMap<String, String>()
        params["page"] = mString
        val getDepartment = RestServiceOld(mBaseActivity)
        viewModelScope.launch(Dispatchers.Main) {
            _status.value = Constant.MarsApiStatus.LOADING
            try {

                val mUserDataModel = getDepartment?.getUserPageAsync(params)?.await()
                if (mUserDataModel?.data?.size!! > 0) {
                    mUserDataModel?.data?.let { localPhotos.addAll(it) }
                    var _localPhotos: ArrayList<DataModel> = localPhotos
                    _photos.value = _localPhotos
                }
                _status.value = Constant.MarsApiStatus.DONE
            } catch (e: Exception) {
                _status.value = Constant.MarsApiStatus.ERROR
                _photos.value = listOf()
            }
        }
    }

    private fun callAdd() {
//        localPhotos[0].firstName = "partha"
        _photos.value = localPhotos
    }

    private fun startRStream() {

//Create an Observable//

        val myObservable = getObservable()

//Create an Observer//

        val myObserver = getObserver()

//Subscribe myObserver to myObservable//

        myObservable
            .subscribe(myObserver)
    }

    private fun getObserver(): Observer<String> {
        return object : Observer<String> {
            override fun onSubscribe(d: Disposable) {
            }

//Every time onNext is called, print the value to Android Studio’s Logcat//

            override fun onNext(s: String) {
                Log.d(TAG, "onNext: $s")
            }

//Called if an exception is thrown//

            override fun onError(e: Throwable) {
                Log.e(TAG, "onError: " + e.message)
            }

//When onComplete is called, print the following to Logcat//

            override fun onComplete() {
                Log.d(TAG, "onComplete")
            }
        }
    }

//Give myObservable some data to emit//

    private fun getObservable(): Observable<String> {
        return Observable.just("1", "2", "3", "4", "5")
    }
}




