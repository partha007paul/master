package com.example.demo.activity.singlearchitechture.fragement.two

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.example.demo.R
import com.example.demo.activity.call_api.adapter_view.OnClick
import com.example.demo.activity.call_api.adapter_view.WeatherAdapter
import com.example.demo.activity.call_api.view_model.ApiViewModel
import com.example.demo.activity.singlearchitechture.fragement.two.model.ShareTwoApiViewModel
import com.example.demo.databinding.FragmentOneFeatureTwoBinding
import com.example.demo.fragment.base.BaseFragment
import com.example.demo.fragment.base.BaseFragmentExample
import com.example.demo.navigation.interfaceNavigation.FeatureTwoNavigation
import kotlinx.android.synthetic.main.fragment_one_feature_two.*
import org.koin.android.ext.android.inject

/**
 * @author MAYCON CARDOSO on 2019-11-20.
 */


class FragmentFeatureTwoScreenOne : BaseFragment(), OnClick {
    lateinit var mShareTwoApiViewModel : ShareTwoApiViewModel
    var binding: FragmentOneFeatureTwoBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       return bind(inflater,container)
    }
    private fun bind(  inflater: LayoutInflater,
                       container: ViewGroup?,): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_one_feature_two, container, false)
        mShareTwoApiViewModel =
            ViewModelProviders.of(requireActivity()).get(ShareTwoApiViewModel::class.java)
        mShareTwoApiViewModel.mBaseActivity = baseActivity!!
        binding!!.lifecycleOwner = this
        binding!!.apiDataFragment = mShareTwoApiViewModel
//        /////////////////////
        binding!!.viewEmployees.adapter = WeatherAdapter(baseActivity!!,this)
        ////////////////
        return binding?.root!!
    }

    override fun position(navPosition: Int) {
        baseActivity?.showToast(""+navPosition)
        mShareTwoApiViewModel?.setValue(navPosition)
        binding?.root?.findNavController()?.navigate(R.id.action_fragmentFeatureOneScreenOne_to_fragmentFeatureTwoScreenTwo)
    }

    override fun onDestroyView() {
        super.onDestroyView()

    }
}