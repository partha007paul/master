package com.example.demo.activity.singlearchitechture.fragement.one

import android.os.Bundle
import android.view.View
import com.example.demo.R
import com.example.demo.fragment.base.BaseFragmentExample
import com.example.demo.navigation.interfaceNavigation.FeatureOneNavigation
import kotlinx.android.synthetic.main.fragment_one_feature_one.*
import org.koin.android.ext.android.inject

/**
 * @author MAYCON CARDOSO on 2019-11-20.
 */
class FragmentFeatureOneScreenOne : BaseFragmentExample<FeatureOneNavigation>() {
    override val navigator: FeatureOneNavigation by inject()

    override fun getLayoutResource() = R.layout.fragment_one_feature_one

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btGoToScreenTwo.setOnClickListener {
            navigator.navigateToScreenTwo()
        }
    }
}