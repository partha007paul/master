package com.example.demo.activity.view_model_factory

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.show_call_api_details.view_model.ShowDetailsViewModel

class ViewModelFactoryMainActivity : BaseActivity() {
    private lateinit var mViewModelFactoryModel:ViewModelFactoryModel
    private lateinit var mViewModelFactory:ViewModelFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_model_factory_main)
        viewModelSet()
    }

    private fun viewModelSet(){
        mViewModelFactory = ViewModelFactory(application,10)
        mViewModelFactoryModel = ViewModelProviders
            .of(this,mViewModelFactory)
            .get(ViewModelFactoryModel::class.java)
    }
}