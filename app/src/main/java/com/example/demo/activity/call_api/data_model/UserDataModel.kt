package com.example.demo.activity.call_api.data_model

import java.util.ArrayList

data class UserDataModel(
    val data: ArrayList<DataModel>,
    val page: Int,
    val per_page: Int,
    val support: Support,
    val total: Int,
    val total_pages: Int
)