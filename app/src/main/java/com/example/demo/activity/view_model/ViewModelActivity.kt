package com.example.demo.activity.view_model

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import com.example.demo.activity.BaseActivity

class ViewModelActivity : BaseActivity(), NewMovieFragment.OnFragmentInteractionListener,
    MovieListFragment.OnFragmentInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_model)


        if (savedInstanceState == null) {
            goToMovieListFragment()
        }
    }

    override fun goToNewMovieFragment() {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.flContent, NewMovieFragment.newInstance())
        transaction.commit()
    }

    override fun goToMovieListFragment() {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.flContent, MovieListFragment.newInstance())
        transaction.commit()
    }
}
