package com.example.demo.activity.start

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.start.model.DataModel
import com.example.demo.activity.start.view_model.DataViewModel
import com.example.demo.databinding.ActivityMainBinding
import java.util.*


class MainActivity : BaseActivity() {

    var binding: ActivityMainBinding? = null
    lateinit var mDataViewModel : DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = bind()

        mDataViewModel.mDataModel?.observe(this@MainActivity,androidx.lifecycle.Observer {
                loginUser : DataModel ->
                println("####${loginUser.name}")


        } )

    }

    private fun bind(): View {
        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)
        mDataViewModel = DataViewModel()
        mDataViewModel.mBaseActivity = this@MainActivity
        binding!!.lifecycleOwner = this@MainActivity
        binding!!.data = mDataViewModel
        mDataViewModel.onCreate()
        return binding!!.root
    }

    override fun onResume() {
        mDataViewModel.onResume()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

}


