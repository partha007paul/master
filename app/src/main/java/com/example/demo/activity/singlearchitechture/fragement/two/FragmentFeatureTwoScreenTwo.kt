package com.example.demo.activity.singlearchitechture.fragement.two

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.demo.R
import com.example.demo.activity.call_api.adapter_view.WeatherAdapter
import com.example.demo.activity.singlearchitechture.fragement.two.model.ShareTwoApiViewModel
import com.example.demo.databinding.FragmentOneFeatureTwoBinding
import com.example.demo.databinding.FragmentTwoFeatureTwoBinding
import com.example.demo.fragment.base.BaseFragment
import com.example.demo.fragment.base.BaseFragmentExample
import com.example.demo.navigation.interfaceNavigation.FeatureTwoNavigation
import kotlinx.android.synthetic.main.fragment_splashscreen.*
import org.koin.android.ext.android.inject

/**
 * @author MAYCON CARDOSO on 2019-11-20.
 */
//class FragmentFeatureTwoScreenTwo :
////    Fragment(){
//    BaseFragmentExample<FeatureTwoNavigation>() {
//
//    override val navigator: FeatureTwoNavigation by inject()
//
//    override fun getLayoutResource() = R.layout.fragment_two_feature_two
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//
//    }
//
//}


class FragmentFeatureTwoScreenTwo : BaseFragment(){
    lateinit var mShareTwoApiViewModel : ShareTwoApiViewModel
    var binding: FragmentTwoFeatureTwoBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return bind(inflater,container)

    }
    private fun bind(  inflater: LayoutInflater,
                       container: ViewGroup?,): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_two_feature_two, container, false)
        mShareTwoApiViewModel =
            ViewModelProviders.of(requireActivity()).get(ShareTwoApiViewModel::class.java)
        mShareTwoApiViewModel.mBaseActivity = baseActivity!!
        binding!!.lifecycleOwner = this
        binding!!.apiDataFragment = mShareTwoApiViewModel
////        /////////////////////
//        binding!!.viewEmployees.adapter = WeatherAdapter(baseActivity!!)
        ////////////////
        return binding?.root!!
    }
}