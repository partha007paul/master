package com.example.demo.activity.singlearchitechture.fragement.two.model



import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.data_model.DataModel
import com.example.demo.interface_all.db.AppDatabase
import com.example.demo.interface_all.db.DataModelDb
import com.example.demo.restservice.RestServiceOld
import com.iserve.askalan.constant.Constant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import java.util.*

open class ShareTwoApiViewModel(application: Application) : AndroidViewModel(application) {

    var count: Int = 0
    lateinit var mBaseActivity: BaseActivity


    ////loder///
    private val _status = MutableLiveData<Constant.MarsApiStatus>()
    val status: LiveData<Constant.MarsApiStatus> = _status


    //////////////model////////

    lateinit var  photosDataModel: DataModel

    ///////////////getlist////////////

    private val _photos = MutableLiveData<ArrayList<DataModel>>()
    val photos: LiveData<ArrayList<DataModel>>
    get() = _photos

    //////////Room DB///////////


//    val database = AppDatabase.invoke(context = mBaseActivity)




    fun onClick(v: View): Unit {
        when (v.id) {
            R.id.btOnclick -> {
                count++
                callApi(count.toString())
            }
            R.id.btOnclickList -> {
//                mBaseActivity.showToast(photosData.value!!.first_name)
                callDb()
            }
            R.id.btGoToScreenTwo-> {

//                v.findNavController().navigate(R.id.action_fragmentFeatureOneScreenOne_to_fragmentFeatureTwoScreenTwo)
            }
        }
    }


    private fun callDb() {

        viewModelScope.launch(Dispatchers.Main) {
            mBaseActivity.showToast(mBaseActivity.application.database?.getStudentDao()?.getAllStudent()?.size.toString())
        }
    }

    private fun callApi(mString: String) {
        val params = HashMap<String, String>()
        params["page"] = mString
        val getDepartment = RestServiceOld(mBaseActivity)
        viewModelScope.launch(Dispatchers.Main) {
            _status.value = Constant.MarsApiStatus.LOADING
            try {

                val mUserDataModel = getDepartment?.getUserPageAsync(params)?.await()
                if (mUserDataModel?.data?.size!! > 0) {
                    var _localPhotos: ArrayList<DataModel> =  arrayListOf()
                    mUserDataModel?.data?.let {
                        _localPhotos.addAll(photos?.value?:arrayListOf())
                        _localPhotos.addAll(it)
                    }

                    _photos.value = _localPhotos
                }
                _status.value = Constant.MarsApiStatus.DONE
            } catch (e: Exception) {
                _status.value = Constant.MarsApiStatus.ERROR
                _photos.value = arrayListOf()
            }
        }
    }


    fun setValue(  mPhotosData:Int){
//        _photosData.value =  photos.value!![mPhotosData]

        photosDataModel =  photos.value!![mPhotosData]
//        AppDatabase.invoke(mBaseActivity).dataDao.insertAll(photosDataModel)
//        insertAll(photosDataModel)
        viewModelScope.launch(Dispatchers.Main) {
            val users = DataModelDb(
                photosDataModel.id,
                photosDataModel.avatar,
                photosDataModel.email,
                photosDataModel.first_name,
                photosDataModel.last_name
            )
            mBaseActivity.application.database?.getStudentDao()?.addMultipleStudent(users)
        }
    }



}




