package com.example.demo.activity.call_api.adapter_view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.data_model.DataModel
import com.example.demo.databinding.ItemRowBinding

open class WeatherAdapter(private val baseActivity: BaseActivity,
private val mOnClick:OnClick):
    ListAdapter<DataModel, WeatherAdapter.WeatherViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<DataModel>() {
        override fun areItemsTheSame(oldItem: DataModel, newItem: DataModel): Boolean {
            return (oldItem.id == newItem.id && oldItem.first_name == newItem.first_name)
        }

        override fun areContentsTheSame(oldItem: DataModel, newItem: DataModel): Boolean {
            return(oldItem.avatar == newItem.avatar && oldItem.first_name == newItem.first_name)
        }
    }


    class WeatherViewHolder(
         val binding: ItemRowBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dataModel: DataModel) {
            binding.setVariable(BR.rowData, dataModel)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemRowBinding.inflate(layoutInflater)
//        val v: View = binding.root
//            v.findViewById<ImageView>(R.id.mars_image).setOnClickListener {
//                baseActivity.showToast(""+binding.rowData!!.id)
//                BaseActivity.mData = binding!!.rowData!!
//                baseActivity.openActivity(baseActivity, ShowDetailsActivity::class.java,false)
//            }
        return WeatherViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val marsPhoto =   getItem(position)
        holder.bind(marsPhoto)
        holder.binding.marsImage.setOnClickListener {
            mOnClick.position(position)
        }
    }

    override fun submitList(list: List<DataModel>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}