package com.example.demo.activity.call_api.data_model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.demo.BR


data class DataModel(
    val id: Int,

    val avatar: String,

    val email: String,

    val last_name: String
): BaseObservable(){

    @Bindable
    var first_name: String = String()
        set(value) {
            field = value
            notifyPropertyChanged(BR.first_name)
        }

//    init {
//        first_name = this.firstName
//    }
}