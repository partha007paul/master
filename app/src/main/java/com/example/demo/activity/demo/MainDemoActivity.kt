package com.example.demo.activity.demo

import android.content.Intent
import android.os.Bundle
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.ApiCallActivity
import com.example.demo.activity.view_model_factory.ViewModelFactoryMainActivity
import kotlinx.android.synthetic.main.activity_main_demo.*


class MainDemoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_demo)

        button2.setOnClickListener {
//            startActivity(
//                Intent(this, ApiCallActivity::class.java)
////                    .setFlags(
////                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
////                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
////                            Intent.FLAG_ACTIVITY_NEW_TASK
////                    )
////                    .set?Flags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
//            )
            openActivity(this, ViewModelFactoryMainActivity::class.java, false)
        }
    }
}