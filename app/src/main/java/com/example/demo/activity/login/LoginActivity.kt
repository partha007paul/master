package com.example.demo.activity.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.login.login_view_model.LoginViewModel
import com.example.demo.databinding.ActivityLoginBinding


class LoginActivity : BaseActivity() {
    var binding: ActivityLoginBinding? = null
    lateinit var mLoginViewModel : LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }

    private fun bind(): View {
        binding = DataBindingUtil.setContentView(this@LoginActivity, R.layout.activity_login)
        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        mLoginViewModel.mBaseActivity = this@LoginActivity
        binding!!.lifecycleOwner = this@LoginActivity
        binding!!.apiData = mLoginViewModel
        ////////////////
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            val selectedMediaUri = data!!.data
            if (selectedMediaUri.toString().contains("image")) {
                println("**Image**")
            } else if (selectedMediaUri.toString().contains("video")) {
                println("**video**")

            }
        }
    }
}