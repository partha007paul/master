package com.example.demo.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.example.demo.activity.call_api.data_model.DataModel
import com.iserve.askalan.abstract_fun.BaseActivityAbstract
import com.example.demo.application.AppApplication
import com.example.demo.activity.crash.CrashActivity
import com.iserve.askalan.constant.Constant
import java.util.regex.Pattern


/**
 * Created by root partha
 */
open class BaseActivity : BaseActivityAbstract() {

    ////////////////////naviagtion drawer end////////////
    var width: Int = 0
    lateinit var application: AppApplication
    var height: Int = 0
    var prsDlg: ProgressDialog? = null

    companion object  {
        lateinit var  mDataModel : DataModel
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        application = getApplication() as AppApplication
        val display = windowManager.defaultDisplay
        height=display.height
        width=display.width
//        setDefaultExceptionHandler()
        prsDlg = ProgressDialog(this)

    }

    override val isNetworkAvailable: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            @SuppressLint("MissingPermission") val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            } else {
            }
            return false

        }

    override val isNetworkAvailableDilog: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            @SuppressLint("MissingPermission") val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            } else {
                AlertDialog.Builder(this@BaseActivity)
                    .setTitle("No Network Connection")
                    .setMessage("Please check your internet connection")
                    .setNeutralButton("Ok") { dialog, which -> dialog.dismiss() }
                    .show()
            }
            return false
        }

    override fun openActivity(caller: Activity, destination: Class<*>, flag: Boolean) {
        startActivity(Intent(caller, destination))
        overridePendingTransition(0, 0);
        if (flag) {
            caller.finish()
        }
    }

    override fun showToast(msg: String) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show()
    }

    fun showPrintData(msg: String) {
        println("##############"+msg)
    }

    fun showLogD(msg: String) {
        Log.d("tagD",msg)
    }

    fun showLogE(msg: String) {
        Log.e("tagE",msg)
    }

    override fun smuitTranslation() {
        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build()
        )
        StrictMode.setVmPolicy(
            StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build()
        )
    }

    override fun onActivityResultSent(requestCode: Int, resultCode: Int, data: Intent) {

    }

    override fun onRequestPermissionsResultSent(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

    }

    override fun setDefaultExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler { thread, e ->
            Log.e("BaseActivity", "Default Exception Handler : ")

            val DOUBLE_LINE_SEP = "\r\n\r\n"
            val SINGLE_LINE_SEP = "\r\n"
            var arr = e.stackTrace
            val report = StringBuffer(e.toString())
            val lineSeparator = "-------------------------------\n\n"
            report.append(DOUBLE_LINE_SEP)
            report.append("--------- Stack trace ---------\n\n")
            for (i in arr.indices) {
                report.append("    ")
                report.append(arr[i].toString())
                report.append(SINGLE_LINE_SEP)
            }

            // If the exception was thrown in a background thread inside
            // AsyncTask, then the actual exception can be found with
            // getCause
            val cause = e.cause
            if (cause != null) {
                report.append(lineSeparator)
                report.append("--------- Cause ---------\n\n")
                report.append(cause.toString())
                report.append(DOUBLE_LINE_SEP)
                arr = cause.stackTrace
                for (i in arr.indices) {
                    report.append("    ")
                    report.append(arr[i].toString())
                    report.append(SINGLE_LINE_SEP)
                }
            }

            System.err.println(report.toString())

            // Getting the Device brand,model and sdk version details.
            report.append(lineSeparator)
            report.append("--------- Device ---------\n\n")
            report.append("Brand: ")
            report.append(Build.BRAND)
            report.append(SINGLE_LINE_SEP)
            report.append("Device: ")
            report.append(Build.DEVICE)
            report.append(SINGLE_LINE_SEP)
            report.append("Model: ")
            report.append(Build.MODEL)
            report.append(SINGLE_LINE_SEP)
            report.append("Metric: ")

            val density = resources.displayMetrics.densityDpi

            when (density) {
                DisplayMetrics.DENSITY_LOW -> report.append("LDPI ")
                DisplayMetrics.DENSITY_MEDIUM -> report.append("MDPI ")
                DisplayMetrics.DENSITY_HIGH -> report.append("HDPI ")
                DisplayMetrics.DENSITY_XHIGH -> report.append("XHDPI ")
            }

            val dm = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(dm)

            report.append(dm.widthPixels.toString() + "x" + dm.heightPixels.toString() + "  " + dm.densityDpi.toString() + "dpi")
            report.append(SINGLE_LINE_SEP)
            report.append("Id: ")
            report.append(Build.ID)
            report.append(SINGLE_LINE_SEP)
            report.append("Product: ")
            report.append(Build.PRODUCT)
            report.append(SINGLE_LINE_SEP)
            report.append(lineSeparator)
            report.append("--------- Firmware ---------\n\n")
            report.append("SDK: ")
            report.append(Build.VERSION.SDK)
            report.append(SINGLE_LINE_SEP)
            report.append("Release: ")
            report.append(Build.VERSION.RELEASE)
            report.append(SINGLE_LINE_SEP)
            report.append("Incremental: ")
            report.append(Build.VERSION.INCREMENTAL)
            report.append(SINGLE_LINE_SEP)
            report.append(lineSeparator)

            //                System.out.println("stacktrace:---" + report.toString());

            val crashedIntent = Intent(this@BaseActivity, CrashActivity::class.java)
            crashedIntent.putExtra("stacktrace", report.toString())
            crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(crashedIntent)
            System.exit(0)
        }
    }

    override fun showProgress() {
        prsDlg!!.setMessage("Please wait...")
        prsDlg!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        prsDlg!!.isIndeterminate = true
        prsDlg!!.setCancelable(false)
        prsDlg!!.show()
    }

    override fun hideProgress() {
        if (null != prsDlg) {
            if (prsDlg!!.isShowing) {
                prsDlg!!.dismiss()
            }
        }
    }

    override fun showKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    override fun hideKeyboard(activity: Activity) {
        var view: View? = activity.findViewById(android.R.id.content)
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        /*
        if (!isKeyboardVisible())
           return;
        */
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view = currentFocus
        if (view == null) {
            if (inputMethodManager.isAcceptingText) {
                try {
                    inputMethodManager.hideSoftInputFromWindow(
                        this.findViewById<View>(android.R.id.content).windowToken,
                        0
                    )
                } catch (e: Exception) {
                    //  MyLog.printStackTrace(e);
                }

                try {
                    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                } catch (e: Exception) {
                    //  MyLog.printStackTrace(e);
                }

                // inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
            }
        } else {
            if (view is EditText) {
                view.setText(view.text.toString()) // reset edit text bug on some keyboards bug
                try {
                    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                } catch (e: Exception) {
                    // MyLog.printStackTrace(e);
                }

                try {
                    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                } catch (e: Exception) {
                    //MyLog.printStackTrace(e);
                }

                // inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
            }
        }
    }

    override fun hideKeyBoard(et: EditText) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(et.windowToken, 0)
    }

    override fun isvalidMailid(mail: String): Boolean {
        return Pattern.compile(Constant.EMAIL_PATTERN).matcher(mail).matches()
    }









}
