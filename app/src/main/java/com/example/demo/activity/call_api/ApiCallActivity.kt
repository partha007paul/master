package com.example.demo.activity.call_api

import com.example.demo.activity.call_api.adapter_view.WeatherAdapter
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.view_model.ApiViewModel
import com.example.demo.databinding.ActivityApiCallBinding
import kotlinx.android.synthetic.main.activity_api_call.*
import java.util.*

class ApiCallActivity : BaseActivity() {
    var binding: ActivityApiCallBinding? = null
    lateinit var mApiViewModel : ApiViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }
    private fun bind(): View {
        binding = DataBindingUtil.setContentView(this@ApiCallActivity, R.layout.activity_api_call)
        mApiViewModel = ViewModelProviders.of(this).get(ApiViewModel::class.java)
        mApiViewModel.mBaseActivity = this@ApiCallActivity
        binding!!.lifecycleOwner = this@ApiCallActivity
        binding!!.apiData = mApiViewModel
        /////////////////////
//        binding!!.viewEmployees.adapter = WeatherAdapter(this@ApiCallActivity,mApiViewModel)
//        binding!!.viewEmployees.adapter = WeatherAdapter(this@ApiCallActivity)
        ////////////////
        return binding!!.root
    }

    
}