package com.example.demo.activity.view_model_factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.demo.application.AppApplication

class ViewModelFactory(val application: Application, private val foo: Int):
    ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return    ViewModelFactoryModel(
            application, foo
        ) as T

//        modelClass.getConstructor(Application::class.java,Int::class.java)
//            .newInstance(application,foo)


    }

}
//    private val arg : Int, application: Application):
//    ViewModelProvider.AndroidViewModelFactory(application) {
//    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        return modelClass.getConstructor(Int::class.java)
//            .newInstance(arg)
//    }
//}