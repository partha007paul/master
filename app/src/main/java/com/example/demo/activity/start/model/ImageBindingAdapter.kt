package com.example.demo.activity.start.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter


object ImageBindingAdapter {
    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageUrl(view: ImageView, resource: Int) {
        view.setImageResource(resource)
    }
}