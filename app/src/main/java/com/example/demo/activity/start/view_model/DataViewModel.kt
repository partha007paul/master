package com.example.demo.activity.start.view_model



import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.ApiCallActivity
import com.example.demo.activity.start.model.DataModel
import java.util.*

open class DataViewModel : BaseObservable() {
    lateinit var mBaseActivity : BaseActivity
    lateinit var loginUser : DataModel
    var i = 0
    var timer: Timer? = null

    @get:Bindable
    var name: String? = null
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }

    @get:Bindable
    var summary: String? = null
        set(summary) {
            field = summary
            notifyPropertyChanged(BR.summary)
        }

    @get:Bindable
    var image: Int? = null
        set(image) {
            field = image
            notifyPropertyChanged(BR.image)
        }

    private var userMutableLiveData: MutableLiveData<DataModel>? = null
    val mDataModel: MutableLiveData<DataModel>?
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData<DataModel>()
            }
            return userMutableLiveData
        }

    fun onClick( v : View) : Unit {
        when(v.id){
            R.id.btClick->{
                timer?.cancel()
                timer = null
                if (i == 0) {
                    name = "Android Lollipop"
                    summary = "Android 5.0"
                    image = R.drawable.lollipop



                    i = 1
                }
                else {
                    name = "Android MarshMallow"
                    summary = "Android 6.0"
                    image = R.drawable.marsh

//                        dataModel =
//                            DataModel(R.drawable.marsh, "Android MarshMallow", "Android 6.0")

                    i = 0

                }
//                    binding?.data = dataModel
                notifyChange()
            }

            R.id.btReset-> {
                if(timer==null) {
                    timer = Timer()
                    timer?.scheduleAtFixedRate(object : TimerTask() {
                        override fun run() {
                            mBaseActivity.runOnUiThread {

                                if (i == 0) {
                                    name = "Android Lollipop"
                                    summary = "Android 5.0"
                                    image = R.drawable.lollipop
                                    loginUser =
                                        DataModel(R.drawable.lollipop, "Android Lollipop", "Android 5.0")
                                    i = 1
                                } else {
                                    name = "Android MarshMallow"
                                    summary = "Android 6.0"
                                    image = R.drawable.marsh
                                    i = 0
                                    loginUser =
                                        DataModel(R.drawable.marsh, "Android MarshMallow", "Android 6.0")
                                }
                                userMutableLiveData!!.value = loginUser
                                notifyChange()
                            }
                        }
                    }, 1000, 1000)
                }
            }

            R.id.btApiCall->{
//                mBaseActivity.openActivity(mBaseActivity, ViewModelActivity::class.java,false)
                mBaseActivity.openActivity(mBaseActivity,ApiCallActivity::class.java,false)
            }

        }
    }

    fun onCreate() {

    }

    init {
        name = "Android Lollipop"
        summary = "Android 5.0"
        image = R.drawable.lollipop
    }

    fun onResume() {

    }


    fun callApi(email: String , password : String): String {

        return "partha"
    }
}



