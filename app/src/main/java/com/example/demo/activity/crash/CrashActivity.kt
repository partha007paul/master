package com.example.demo.activity.crash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import com.example.demo.R
import com.example.demo.activity.BaseActivity

class CrashActivity : BaseActivity() {
    private var bt_sent: Button? = null
    private var reportTextView: TextView? = null
    internal lateinit var stackTrace: String
    override fun onCreate(savedInstanceState: Bundle?) {
        smuitTranslation()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error_report)
        bt_sent = findViewById(R.id.bt_sent)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        stackTrace = intent.getStringExtra("stacktrace")!!
        reportTextView = findViewById<View>(R.id.tvError) as TextView
        println(stackTrace)
        reportTextView!!.text = stackTrace
        bt_sent!!.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "partha.paul007@gmail.com", null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EasyAppo Crash Report")
            emailIntent.putExtra(Intent.EXTRA_TEXT, reportTextView!!.text.toString())
            startActivity(Intent.createChooser(emailIntent, "Send Error to ..."))
        }
    }

    fun clickedSend(view: View) {
        val t = findViewById<View>(R.id.tvError) as TextView

        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "partha.paul007@gmail.com", null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EasyAppo Crash Report")
        emailIntent.putExtra(Intent.EXTRA_TEXT, t.text.toString())
        startActivity(Intent.createChooser(emailIntent, "Send Error to ..."))
    }


}
