package com.example.demo.activity.call_api.data_model

data class Support(
    val text: String,
    val url: String
)