package com.example.demo.activity.singlearchitechture.fragement.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.demo.R
import com.example.demo.fragment.base.BaseFragment
import com.example.demo.fragment.base.BaseFragmentExample
import com.example.demo.navigation.interfaceNavigation.SplashScreenNavigation
import kotlinx.android.synthetic.main.fragment_splashscreen.*
import kotlinx.android.synthetic.main.fragment_splashscreen.view.*
import org.koin.android.ext.android.inject

/**
 * @author MAYCON CARDOSO on 2019-11-20.
 */
//class FragmentSplashScreen
//    :   BaseFragment() {
//    override fun getLayoutResource() = R.layout.fragment_splashscreen
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        btGoFeatureOne.setOnClickListener { v->
//            v.findNavController().navigate(R.id.action_global_feature_one)
////            v.findNavController().navigate(R.id.action_global_feature_two)
//        }
//
//
//    }
//}


class FragmentSplashScreen
    : BaseFragmentExample<SplashScreenNavigation>() {

    override val navigator: SplashScreenNavigation by inject()

    override fun getLayoutResource() = R.layout.fragment_splashscreen

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btGoFeatureOne.setOnClickListener {
            navigator.navigateToFeatureOne()
        }

        btGoFeatureTwo.setOnClickListener {
            navigator.navigateToFeatureTwo()
        }
    }
}