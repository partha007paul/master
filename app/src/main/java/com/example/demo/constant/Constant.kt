package com.iserve.askalan.constant

import java.util.regex.Pattern

/**
 * Created by root on 17/8/15.
 */
open class Constant {


    enum class MarsApiStatus { LOADING, ERROR, DONE }
    enum class Userdata {
        USER_PREF, SESSION, USER_LOGIN, CATEGORY_LIST, SEARCH, sToken
    }
    companion object {
        private const val mProductionEnable = false
        //        val mProURL = "http://admin-development.askalan.com.my/"
        private const val mProURL = "https://reqres.in/"
        private const val mUatURL = "https://reqres.in/"
        //        val mUatURL = "http://admin-development.askalan.com.my/"
        val BASE_URL = if (mProductionEnable) mProURL else mUatURL;
        val EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

        fun isEmailValid(email: String): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }
    }

}
