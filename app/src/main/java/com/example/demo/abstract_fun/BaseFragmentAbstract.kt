package com.iserve.askalan.abstract_fun

import androidx.fragment.app.Fragment


/**
 * Created by android on 19/3/18.
 */

abstract class BaseFragmentAbstract : Fragment() {
    abstract fun displayView(fragment: Fragment)
    abstract fun setDisplayView(fragment: Fragment)
}
