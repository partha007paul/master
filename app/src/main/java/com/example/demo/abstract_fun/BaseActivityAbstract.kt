package com.iserve.askalan.abstract_fun

import android.app.Activity
import android.content.Intent
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlin.jvm.JvmField as JvmField1

/**
 * Created by android on 19/3/18.
 */

abstract class BaseActivityAbstract : AppCompatActivity() {

    abstract val isNetworkAvailable: Boolean
    abstract val isNetworkAvailableDilog: Boolean
    abstract fun smuitTranslation()
    abstract fun showProgress()
    abstract fun hideProgress()
    abstract fun showKeyboard()
    abstract fun hideKeyboard(activity: Activity)
    abstract fun hideKeyBoard(et: EditText)
    abstract fun isvalidMailid(mail: String): Boolean
    abstract fun openActivity(caller: Activity, destination: Class<*>, flag: Boolean)
    protected abstract fun setDefaultExceptionHandler()
    abstract fun showToast(msg: String)
    abstract fun onActivityResultSent(requestCode: Int, resultCode: Int, data: Intent)
    abstract fun onRequestPermissionsResultSent(requestCode: Int, permissions: Array<String>, grantResults: IntArray)


}
