package com.example.demo.interface_all.db

import androidx.room.*


@Dao
interface DataDao {

    @Insert
    fun saveStudent(student: DataModelDb)

    @Query("SELECT * FROM dataModel ")//ORDER BY id ASC
    suspend fun getAllStudent() : List<DataModelDb>

    @Insert
    suspend fun addMultipleStudent(vararg student: DataModelDb)

    @Update
    suspend fun updateStudent(student: DataModelDb)

    @Delete
    suspend fun deleteStudent(student: DataModelDb)
}