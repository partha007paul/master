package com.example.demo.interface_all.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(
        entities = [DataModelDb::class],
        version = 1
)
abstract class AppDatabase : RoomDatabase() {

        abstract fun getStudentDao() : DataDao

        companion object{

                @Volatile private var instance : AppDatabase? = null
                private val Lock = Any()

                operator fun invoke (context: Context) = instance ?: synchronized(Lock){
                        instance ?: buildDb(context).also {
                                instance = it
                        }
                }

                private fun buildDb(context: Context) = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "studentdb"
                ).build()

        }
}
