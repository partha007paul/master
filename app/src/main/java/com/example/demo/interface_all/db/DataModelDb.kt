package com.example.demo.interface_all.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "dataModel")
data class DataModelDb(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo
    val avatar: String,

    @ColumnInfo
    val email: String,

    @ColumnInfo
    val firstName: String,

    @ColumnInfo (name= "last_name")
    val lastName: String
)