//package com.example.demo.fragment.root
//
//import android.app.Activity
//import android.os.Bundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.fragment.app.Fragment
//import com.example.demo.R
//import com.example.demo.activity.BaseActivity
//import com.example.demo.fragment.base.BaseFragment
//import androidx.fragment.app.FragmentTransaction
//import com.example.demo.fragment.demo.FragmentBroadcastListInfo
//
//
///**
// * Created by root on 21/7/15.
// */
//class RootFragmentGustDashbord : BaseFragment() {
//
//    private var base: BaseActivity? = null
//    private var fragmentTransaction: FragmentTransaction? = null
//
//
//    override fun onAttach(activity: Activity) {
//        super.onAttach(activity)
//        base = activity as BaseActivity?
//    }
////
////    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
////
////        val v = inflater.inflate(R.layout.root_fragment, null, false)
////
////        val fragment = childFragmentManager.findFragmentById(R.id.flRoot)
////        if (fragment == null) {
////            val fragment1 = FragmentGustBrowse()
////            setDisplayView(fragment1)
////            childFragmentManager.beginTransaction().add(R.id.flRoot, fragment1).commit()
////
////        }
////
////        return v
////    }
//
////    override fun displayView(fragment: Fragment) {
////        val fragmentManager = childFragmentManager
////        fragmentManager.beginTransaction()
////            .replace(R.id.flRoot, fragment).addToBackStack(null).commit()
////    }
//
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//
//        val v = inflater.inflate(R.layout.root_fragment, null, false)
//        val fragment = childFragmentManager.findFragmentById(R.id.flRoot)
//        if (fragment == null) {
//            val fragmentContactInfo = FragmentBroadcastListInfo()
//            setDisplayView(fragmentContactInfo)
//            childFragmentManager.beginTransaction().add(R.id.flRoot, fragmentContactInfo).commit()
//
//        }
//
//        return v
//    }
//
//    override fun displayView(fragment: Fragment) {
//        val fragmentManager = childFragmentManager
//        fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction!!.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
//        if(fragment!=null) {
//            fragmentTransaction!!.replace(R.id.flRoot, fragment!!).addToBackStack(null).commit()
//        }
//    }
//}
