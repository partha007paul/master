package com.example.demo.fragment.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.demo.R
import com.example.demo.activity.BaseActivity
import com.example.demo.activity.call_api.adapter_view.WeatherAdapter
import com.example.demo.activity.call_api.view_model.ApiViewModel
import com.example.demo.activity.singlearchitechture.fragement.two.model.ShareTwoApiViewModel
import com.example.demo.databinding.FragmentOneFeatureTwoBinding
import com.example.demo.databinding.FragmentTwoFeatureTwoBinding
import java.sql.Types.NULL

/**
 * @author MAYCON CARDOSO on 2019-11-20.
 */
abstract class BaseFragmentExample<NAV : Navigation> : Fragment() {

    abstract val navigator: NAV
    abstract fun getLayoutResource(): Int

    //////////////////////////////////////
    lateinit var mShareTwoApiViewModel : ShareTwoApiViewModel
    var binding: FragmentOneFeatureTwoBinding? = null
    var binding1: FragmentTwoFeatureTwoBinding? = null


    ////////////////////////////////////////////////

    var baseActivity: BaseActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity = (context as BaseActivity?)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return bind(inflater,container)?:inflater.inflate(getLayoutResource(), container, false)

    }



    private fun bind(  inflater: LayoutInflater,
                       container: ViewGroup?,):
            View? {
        var mView : View? = inflater.inflate(getLayoutResource(), container, false)
//        binding = DataBindingUtil.inflate(inflater, getLayoutResource(), container, false)
//
//
//        if(binding!=null) {
//            mShareTwoApiViewModel =
//                ViewModelProviders.of(requireActivity()).get(ShareTwoApiViewModel::class.java)
//            mShareTwoApiViewModel.mBaseActivity = baseActivity!!
//            binding!!.lifecycleOwner = this
//            binding!!.apiDataFragment = mShareTwoApiViewModel
////        /////////////////////
//            binding!!.viewEmployees.adapter = WeatherAdapter(baseActivity!!)
//            ////////////////
//            return binding?.root!!
//        }
//        binding1 = DataBindingUtil.inflate(inflater, getLayoutResource(), container, false)
//        if(binding1!=null) {
//            mShareTwoApiViewModel =
//                ViewModelProviders.of(requireActivity()).get(ShareTwoApiViewModel::class.java)
//            mShareTwoApiViewModel.mBaseActivity = baseActivity!!
//            binding1!!.lifecycleOwner = this
//            binding1!!.apiDataFragment = mShareTwoApiViewModel
////        /////////////////////
////            binding1!!.viewEmployees.adapter = WeatherAdapter(baseActivity!!)
//            ////////////////
//            return binding1?.root!!
//
//        }
        return mView?.rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.bind(findNavController())
    }

    override fun onDestroy() {
        navigator.unbind()
        super.onDestroy()
    }

    ////////////////////////////////////



}