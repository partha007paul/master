package com.example.demo.fragment.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.demo.activity.BaseActivity


/**
 * Created by root on 21/7/15.
 */
abstract class BaseFragment : Fragment() {
//    abstract fun getLayoutResource(): Int
    var baseActivity: BaseActivity? = null



    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity = (context as BaseActivity?)!!
    }

//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        return inflater.inflate(getLayoutResource(), container, false)
//    }

//    override fun displayView(fragment: Fragment) {
//        if (fragment != null) {
//            (parentFragment as BaseFragment).displayView(fragment)
//        }
//    }
//
//
//    override fun setDisplayView(fragment: Fragment) {
//
//    }
//
//    fun goBack() {
//        (activity as BaseActivity).onBackPressed()
//    }


}
