package com.example.demo.fragment.base

import androidx.navigation.NavController

/**
 * @author MAYCON CARDOSO on 2019-11-21.
 */
interface Navigation {
    fun bind(navController: NavController)
    fun unbind()
}